(ns plf02.core)

;####################################################################################################################
;                                                   PREDICADO
;####################################################################################################################

(defn funcion-asosiative?-1
  [x] (associative? x))

(defn funcion-asosiative?-2
  [x] (associative? x))

(defn funcion-asosiative?-3
  [x] (associative? x))

(funcion-asosiative?-1 [1 2 3 4 5 6])
(funcion-asosiative?-2 '(1 2 3))
(funcion-asosiative?-3 {:a 1 :b 2})



(defn funcion-boolean?-1
  [x] (boolean? x))

(defn funcion-boolean?-2
  [x] (boolean? x))

(defn funcion-boolean?-3
  [x] (boolean? x))

(funcion-boolean?-1 0)
(funcion-boolean?-2 false)
(funcion-boolean?-3 true)


(defn funcion-char?-1
  [x] (char? x))

(defn funcion-char?-2
  [x] (char? x))

(defn funcion-char?-3
  [x] (char? x))

(funcion-char?-1 \a)
(funcion-char?-2 #{\a})
(funcion-char?-3 0)



(defn función-coll?-1
  [x]
  (coll? x))

(defn función-coll?-2
  [x y]
  (coll? (+ x y)))

(defn función-coll?-3
  [x]
  (coll? x))

(función-coll?-1 [1 2 3])
(función-coll?-2 3 4)
(función-coll?-3 '("columna 1" "columna2"))





(defn función-decimal?-1
  [x y]
  (decimal? (+ x y)))

(defn función-decimal?-2
  [x]
  (decimal? x))

(defn función-decimal?-3
  [x]
  (decimal? x))

(función-decimal?-1 1M  3)
(función-decimal?-2 "hola")
(función-decimal?-3 [1 2 3 4])




(defn función-double?-1
  [x]
  (double? x))

(defn función-double?-2
  [x y]
  (double? (- x y)))

(defn función-double?-3
  [x]
  (double? (first x)))


(función-double?-1 1.0)
(función-double?-2 5.5 3)
(función-double?-3 [2.0 5.0 1.0])




(defn función-float?-1
  [x]
  (float? x))

(defn función-float?-2
  [x y]
  (float? (* x y)))

(defn función-float?-3
  [x y]
  (float? (/ x y)))

(función-float?-1 1.0)
(función-float?-2 3 2.1)
(función-float?-3 4 5)






(defn función-ident?-1
  [x]
  (ident? x))

(defn función-ident?-2
  [x]
  (ident? x))

(defn función-ident?-3
  [x y]
  (ident? (conj x y)))

(función-ident?-1 'abc)
(función-ident?-1 :a)
(función-ident?-3 [1 2 3] :b)





(defn función-indexed?-1
  [x]
  (indexed? x))

(defn función-indexed?-2
  [x y]
  (indexed? (x y)))

(defn función-indexed?-3
  [x]
  (indexed? x))

(función-indexed?-1 [1 2 3])
(función-indexed?-2 [:b ":u" :a "cadena" :c "efe"] 1)
(función-indexed?-3 [1 2 3 {:a 1 :b 2}])




(defn función-int?-1
  [x]
  (int? x))

(defn función-int?-2
  [x y]
  (int? (+ x y)))

(defn función-int?-3
  [x y]
  (int? (/ x y)))

(función-int?-1 25.0)
(función-int?-2 4 9)
(función-int?-3 1 4)




(defn función-integer?-1
  [x]
  (integer? x))

(defn función-integer?-2
  [x y z]
  (integer? (+ x y z)))

(defn función-integer?-3
  [x]
  (integer? (count x)))

(función-integer?-1 10)
(función-integer?-2 3 6 8)
(función-integer?-3 [\A \B \C])




(defn función-keyword?-1
  [x]
  (keyword? x))

(defn función-keyword?-2
  [x]
  (keyword? x))

(defn función-keyword?-3
  [x]
  (keyword? x))


(función-keyword?-1 :a_w)
(función-keyword?-2 {:a 1 :b 2 :c 10})
(función-keyword?-3 :a)




(defn función-list?-1
  [x]
  (list? x))

(defn función-list?-2
  [x]
  (list? (range x)))

(defn función-list?-3
  [x y]
  (list? (conj x y)))

(función-list?-1 '(1 2 3 4 5 6))
(función-list?-2 10)
(función-list?-3 '("hola" "adios") '(1 2 3 4))





(defn función-map-entry?-1
  [x]
  (map-entry? x))

(defn función-map-entry?-2
  [x]
  (map-entry? x))

(defn función-map-entry?-3
  [x]
  (map-entry? x))

(función-map-entry?-1 #{1 2 3 4})
(función-map-entry?-2 {:c \C})
(función-map-entry?-3 {{3 2} {2 3} {6 1} {4 5}})





(defn función-map?-1
  [x]
  (map? x))

(defn función-map?-2
  [x]
  (map? x))

(defn función-map?-3
  [x]
  (map? x))

(función-map?-1 {:a "hola" :b "20"})
(función-map?-2 [1 2 3 4 5])
(función-map?-3 "hola")




(defn función-nat-int?-1
  [x]
  (nat-int? x))

(defn función-nat-int?-2
  [x y]
  (nat-int? (+ x y)))

(defn función-nat-int?-3
  [x y]
  (nat-int? (/ x y)))

(función-nat-int?-1 999)
(función-nat-int?-2 999 -1000)
(función-nat-int?-3 50 10)




(defn función-number?-1
  [a]
  (number? a))

(defn función-number?-2
  [a]
  (number? a))

(defn función-number?-3
  [a]
  (number? a))

(función-number?-1 10)
(función-number?-2 \a)
(función-number?-3 :a)





(defn función-pos-int?-1
  [a]
  (pos-int? a))

(defn función-pos-int?-2
  [a]
  (pos-int? a))

(defn función-pos-int?-3
  [a]
  (pos-int? a))

(función-pos-int?-1 21345)
(función-pos-int?-2 -100)
(función-pos-int?-3 [100 23 -1])




(defn función-ratio?-1
  [x]
  (ratio? x))

(defn función-ratio?-2
  [x]
  (ratio? x))

(defn función-ratio?-3
  [x]
  (ratio? x))


(función-ratio?-1 5/4)
(función-ratio?-2 [ 3/4])
(función-ratio?-3 :a)






(defn función-rational?-1
  [x]
  (rational? x))

(defn función-rational?-2
  [x]
  (rational? x))

(defn función-rational?-3
  [x]
  (rational? x))

(función-rational?-1 10)
(función-rational?-2 2/1)
(función-rational?-3 0.10)






(defn función-seq?-1
  [x]
  (seq? x))

(defn función-seq?-2
  [x]
  (seq? x))

(defn función-seq?-3
  [x]
  (seq? x))

(función-seq?-1 '(1 2 3))
(función-seq?-2 10)
(función-seq?-3 ["1" "2" "3"])






(defn función-seqable?-1
  [x]
  (seqable? x))

(defn función-seqable?-2
  [x]
  (seqable? x))

(defn función-seqable?-3
  [x]
  (seqable? x))

(función-seqable?-1 [])
(función-seqable?-2 '(1 2 3 4))
(función-seqable?-3 ["uno" "dos" "tres"])




(defn función-sequential?-1
  [x]
  (sequential? x))

(defn función-sequential?-2
  [x]
  (sequential? x))

(defn función-sequential?-3
  [x]
  (sequential? x))

(función-sequential?-1 [1 2 3 4 5 6])
(función-sequential?-2 '(1 2 3 4 5))
(función-sequential?-3 #{1 2 3 4 5})




(defn función-set?-1
  [x]
  (set? x))

(defn función-set?-2
  [x]
  (set? x))

(defn función-set?-3
  [x]
  (set? x))

(función-set?-1 #{1 2 3 4})
(función-set?-2 '(1 2 3))
(función-set?-3 [1 2 3 4])






(defn función-some?-1
  [x]
  (some? x))


(defn función-some?-2
  [x]
  (some? x))

(defn función-some?-3
  [x]
  (some? x))

(función-some?-1 45)
(función-some?-2 \a)
(función-some?-3 [])





(defn función-string?-1
  [x]
  (string? x))

(defn función-string?-2
  [x]
  (string? x))

(defn función-string?-3
  [x]
  (string? x))

(función-string?-1 "hola")
(función-string?-2 ["Alexis" "Morales" "Martinez"])
(función-string?-3 \a)




(defn función-symbol?-1
  [x]
  (symbol? x))

(defn función-symbol?-2
  [x]
  (symbol? x))

(defn función-symbol?-3
   [x]
  (symbol? x))

(función-symbol?-1 'z)
(función-symbol?-2 ['a 'b 'c 'd])
(función-symbol?-3 9)



(defn función-vector?-1
  [x]
  (vector? x))

(defn función-vector?-2
  [x]
  (vector? x))

(defn función-vector?-3
  [x]
  (vector? x))

(función-vector?-1 ["hola" "buenas noches"])
(función-vector?-2 (vector 1 2 3))
(función-vector?-2 '(1 2 3 4 5 6 7))

;####################################################################################################################
;                                        FUNCIONES DE ORDEN SUPERIOR
;####################################################################################################################

(defn funcion-drop-1
  [x y] (drop x y))

(defn funcion-drop-2
  [x y] (drop x y))

(defn funcion-drop-3
  [x y] (drop x y))

(funcion-drop-1 3 [75 1 9 4 2 9 1])
(funcion-drop-2 4 '(5 3 1 7 9 2 5 2 1))
(funcion-drop-3 2 #{\a \b \c \d \e})




(defn funcion-drop-last-1
  [x] (drop-last x))

(defn funcion-drop-last-2
  [x y] (drop-last x y))

(defn funcion-drop-last-3
  [x y] (drop-last x y))

(funcion-drop-last-1 [12 3 4 5 6])
(funcion-drop-last-2 2 '("uno" "dos" "tres" "cuatro" "cinco" "seis"))
(funcion-drop-last-3 2 [1 3 5 6 2 3])




(defn funcion-dropWhile-1
  [x y] (drop-while x y))

(defn funcion-dropWhile-2
  [x y] (drop-while x y))

(defn funcion-dropWhile-3
  [x y] (drop-while x y))

(funcion-dropWhile-1 #(> 3 %) [-1 1 1 1 2 3 4])
(funcion-dropWhile-2 #(> 2 %) [1 1 1 1 -2 4 4])
(funcion-dropWhile-3 #(> 4 %) '(1 2 3 4 56 7))





(defn funcion-every?-1
  [x] (every? even? x))

(defn funcion-every?-2
  [x] (every? even? x))

(defn funcion-every?-3
  [x] (every? even? x))

(funcion-every?-1 '(2 4 6))
(funcion-every?-2 [1 2 3 4])
(funcion-every?-3 #{2 4 6 8 10})






(defn funcion-filterv-1
  [x y] (filterv x y))

(defn funcion-filterv-2
  [x y] (filterv x y))

(defn funcion-filterv-3
  [x y] (filterv x y))

(funcion-filterv-1 odd? (range 10))
(funcion-filterv-2 even? '(1 2 3 4 5 6 7 8))
(funcion-filterv-3 even? #{10 20 2 13 33 7})





(defn funcion-groupBy-1
  [x y] (group-by x y))

(defn funcion-groupBy-2
  [x y] (group-by x y))

(defn funcion-groupBy-3
  [x y] (group-by x y))

(funcion-groupBy-1 odd? (range 10))
(funcion-groupBy-2 odd? '(1 2 5 6 7 8 10 13 262 33))
(funcion-groupBy-3 :user-id
                    [{:user-id 1 :uri "/"}
                     {:user-id 2 :uri "/foo"}
                     {:user-id 1 :uri "/account"}])




(defn función-iterate-1
  [a b]
  (iterate a b))

(defn función-iterate-2
  [a b]
  (iterate a b))

(defn función-iterate-3
  [a b]
  (iterate a b))

(función-iterate-1 inc 5)
(función-iterate-2 inc 2)
(función-iterate-3 dec 2)


 

(defn función-keep-1
  [a b]
  (keep a b))

(defn función-keep-2
  [a b]
  (keep a b))

(defn función-keep-3
  [a b]
  (keep a b))

(función-keep-1 even? (range 2 10))
(función-keep-2 odd? (range 30 31))
(función-keep-3 number? (range 20 23))


(defn función-keep-indexed-1
  [a b]
  (keep-indexed a b))

(defn función-keep-indexed-2
  [a b]
  (keep-indexed a b))

(defn función-keep-indexed-3
  [a b]
  (keep-indexed a b))

(función-keep-indexed-1 vector [:a :b :c :d])
(función-keep-indexed-2 list [-9 0 29 -7 45 3 -8])
(función-keep-indexed-3 hash-map [1 2 3 4 5 6])


(defn función-map-indexed-1
  [a b]
  (map-indexed a b))

(defn función-map-indexed-2
  [a b]
  (map-indexed a b))

(defn función-map-indexed-3
  [a b]
  (map-indexed a b))

(función-map-indexed-1  hash-set [1 3 5 6 8 2])
(función-map-indexed-2  list [:a :b :c])
(función-map-indexed-3  vector "REPL")




(defn función-mapcat-1
  [a b]
  (mapcat a b))

(defn función-mapcat-2
  [a b]
  (mapcat a b))

(defn función-mapcat-3
  [a b]
  (mapcat a b))

(función-mapcat-1  reverse [[3 2 1 0] [6 5 4] [9 8 7]])
(función-mapcat-2  reverse [[1 2] [2 2] [2 3]])
(función-mapcat-3  (juxt inc dec) [1 2 3 4])



(defn funcion-mapv-1
  [x y z] (mapv x y z))

(defn funcion-mapv-2
  [x y z] (mapv x y z))

(defn funcion-mapv-3
  [x y z] (mapv x y z))

(funcion-mapv-1 + [1 2 3] [4 5 6])
(funcion-mapv-2 + [1 2 3] '(4 5 6))
(funcion-mapv-3 + [1 2 3] (range 3))





(defn funcion-merge-with-1
  [x y z] (merge-with x y z))

(defn funcion-merge-with-2
  [x y z] (merge-with x y z))

(defn funcion-merge-with-3
  [x y z] (merge-with x y z))

(funcion-merge-with-1 + {:a 1 :b 3 :c 4} {:a 4 :b 2 :c 5})
(funcion-merge-with-2 + {:a 1 :b 3 :c 4} {:a -4 :b -2 :c -5})
(funcion-merge-with-3 into {"uno" ["a" "b" "c"] "dos" ["d" "e" "f"]}
                           {"uno" ["1" "2" "3"] "dos" ["4" "5" "6"]})




(defn funcion-no-any?-1
  [x y] (not-any? x y))

(defn funcion-no-any?-2
  [x y] (not-any? x y))

(defn funcion-no-any?-3
  [x y] (not-any? x y))

(funcion-no-any?-1 odd? '(4 2 6))
(funcion-no-any?-2 odd? (range 10))
(funcion-no-any?-3 odd? [2 4 6])




(defn funcion-not-every?-1
  [x y] (not-every? x y))

(defn funcion-not-every?-2
  [x y] (not-every? x y))

(defn funcion-not-every?-3
  [x y] (not-every? x y))

(funcion-not-every?-1 odd? '(2 4 1 8))
(funcion-not-every?-2 odd? [1 3 5])
(funcion-not-every?-3 odd? (range 10))




(defn funcion-parttition-by-1
  [x y]
  (partition-by y x))

(defn funcion-parttition-by-2
  [x y]
  (partition-by y x))

(defn funcion-parttition-by-3
  [x y]
  (partition-by y x))

(funcion-parttition-by-1 [1 2 3 4 5 6 7 8 9] #(= 2 %))
(funcion-parttition-by-2 (range 10) #(= 4 %))
(funcion-parttition-by-3 '(1 2 3 4 5) #(= 3 %))





(defn función-reduce-kv-1
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-2
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-3
  [a b c]
  (reduce-kv a b c))

(función-reduce-kv-1 assoc {} {:a 3 :b 0 :c 1})
(función-reduce-kv-2 assoc {} {:d 4 :e 8 :f 6})
(función-reduce-kv-3 vector [{:a 1 :b 4}] [{:a 3 :b 4}])




(defn funcion-remove-1
  [x y] (remove x y))

(defn funcion-remove-2
  [x y] (remove x y))

(defn funcion-remove-3
  [x y] (remove x y))

(funcion-remove-1 pos? [1 -2 2 -1 3 7 0])
(funcion-remove-2 neg? [1 -2 2 -1 3 7 0])
(funcion-remove-3 pos? '(1 -2 2 -1 3 7 0))





(defn funcion-reverse-1
  [x] (reverse x))

(defn funcion-reverse-2
  [x] (reverse x))

(defn funcion-reverse-3
  [x] (reverse x))

(funcion-reverse-1 [1 2 3 4 5 6])
(funcion-reverse-1 '(1 2 3 4 5 6))
(funcion-reverse-1 (range 6))

(defn funcion-some-1
  [x y] (some x y))

(defn funcion-some-2
  [x y] (some x y))

(defn funcion-some-3
  [x y] (some x y))

(funcion-some-1 even? '(1 2 3 4))
(funcion-some-2 even? [1 2 3 4])
(funcion-some-3 even? (range 4))



(defn funcion-short-by-1
  [x y] (sort-by x y))

(defn funcion-short-by-2
   [x y] (sort-by x y))

(defn funcion-short-by-3
  [x y] (sort-by x y))

(funcion-short-by-1 count ["aaa" "bb" "c"])
(funcion-short-by-3 count #{"aaa" "bb" "c"})
(funcion-short-by-2 count '("aaa" "bb" "c"))




(defn funcion-split-with-1
  [x y] (split-with x y))

(defn funcion-split-with-2
  [x y] (split-with x y))

(defn funcion-split-with-3
  [x y] (split-with x y))

(funcion-split-with-1 (partial >= 3) [1 2 3 4 5])
(funcion-split-with-2 (partial >= 3) '(1 2 3 4 5))
(funcion-split-with-3 (partial >= 3) (range 6))



(defn funcion-take-1
  [x y] (take x y))

(defn funcion-take-2
  [x y] (take x y))

(defn funcion-take-3
  [x y] (take x y))

(funcion-take-1 3 '(1 2 3 4 5 6))
(funcion-take-2 5 [1 2 3 4 5 6])
(funcion-take-3 4 (range 10))





(defn funcion-take-last-1
  [x y] (take-last x y))

(defn funcion-take-last-2
  [x y] (take-last x y))

(defn funcion-take-last-3
  [x y] (take-last x y))

(funcion-take-last-1 3 '(1 2 3 4 5 6))
(funcion-take-last-2 5 [1 2 3 4 5 6])
(funcion-take-last-3 4 (range 10))






(defn funcion-take-nth-1
  [x y] (take-nth x y))

(defn funcion-take-nth-2
  [x y] (take-nth x y))

(defn funcion-take-nth-3
  [x y] (take-nth x y))

(funcion-take-nth-1 3 '(0 1 2 3 4 5 6))
(funcion-take-nth-2 2 [0 1 2 3 4 5 6])
(funcion-take-nth-3 3 (range 30))



(defn funcion-take-while-1
  [x y] (take-while x y))

(defn funcion-take-while-2
  [x y] (take-while x y))

(defn funcion-take-while-3
  [x y] (take-while x y))

(funcion-take-while-1 neg? [-2 -1 0 1 2 3])
(funcion-take-while-1 pos? '(1 1 1 3 4 -2 -1 0 1 2 3))
(funcion-take-while-1 #(> 3 %) [-2 -1 0 1 2 3 4])





(defn función-update-1
  [a b c]
  (update a b c))

(defn función-update-2
  [a b c]
  (update a b c))

(defn función-update-3
  [a b c]
  (update a b c))

(función-update-1 [1 2 3] 0 inc)
(función-update-2 [10 20 30] 1 inc)
(función-update-3 [100 200 300] 1 inc)


(defn función-update-in-1
  [a b c]
  (update-in a b c))

(defn función-update-in-2
  [a b c d e]
  (update-in a b c d e))

(defn función-update-in-3
  [a b c]
  (update-in a b c))

(función-update-in-1 {1 {:b 3}} [1 :b] inc)
(función-update-in-2 {:a 3} [:a] * 2 2)
(función-update-in-3 [1 {:a 3 :b 3 :c 4}] [1 :c] (fnil dec 1) )